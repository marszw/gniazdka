package tb.sockets.server;

import java.util.ArrayList;

import tb.sockets.utilities.GameFinish;
import tb.sockets.utilities.LetterDecision;
import tb.sockets.utilities.PartyInfo;
import tb.sockets.utilities.PointTable;
import tb.sockets.utilities.WordDecision;

public class Statistics {
	enum DECISIONS { 
		LETTER_CORRECT,
		LETTER_USED,
		LETTER_NOTCORRECT,
		WORD_CORRECT,
		WORD_NOTCORRECT;
	} 
	static private DECISIONS checkWord(String szGivenWord, WordDecision DWord)
	{
		if(szGivenWord.equals(DWord.szGivenWord))
			return DECISIONS.WORD_CORRECT;
		else 
			return DECISIONS.WORD_NOTCORRECT;
	}
	static private int[] retPosOfLetter(String szGivenWord, char Letter)
	{
		ArrayList<Integer> arrPositions = new ArrayList<Integer>();
		char[] chGivenWord = szGivenWord.toCharArray();
		for(int i = 0 ; i < chGivenWord.length; i++ )
		{
			if(Letter == chGivenWord[i]) {
			 arrPositions.add(i);
			}
		}
		
		int[] toRet = new int[arrPositions.size()];
		
		for(int i = 0 ; i < arrPositions.size(); i++ )
			toRet[i] = arrPositions.get(i);
		
		return toRet; 
	}
	static private DECISIONS checkLetterCorrect(String szGivenWord, char Letter, char[] chUsedLetters)
	{
		for(char i : chUsedLetters)
		{
			if(i == Letter)
				return DECISIONS.LETTER_USED;
		}
		
		char[] chGivenWord = szGivenWord.toCharArray();
		for(char i: chGivenWord)
		{
			if(i == Letter)
				return DECISIONS.LETTER_CORRECT; //JEST TAKA LITERA
		}
		
		return DECISIONS.LETTER_NOTCORRECT; //NIE MA TAKIEJ LITERY
	}
	static public String generateDashes(int NmbOfDashes)
	{
		StringBuilder buildStr = new StringBuilder();
		
		for(int i = 0 ; i < NmbOfDashes; i++)
			buildStr.append(" _ ");
		
		return buildStr.toString();
	}
	static public void computePoints(PartyInfo CurrentParty, String szGivenWord)
	{
		
		CurrentParty.iPoints = szGivenWord.length() * 2;
		CurrentParty.iFailures = 0;
		CurrentParty.maxFailures = (int)(szGivenWord.length() * 1.5);
		CurrentParty.iNmbLetters = szGivenWord.length();
		return;
	}
	static public String weldTwoStrings(String A, String B)
	{
		StringBuilder buildStr = new StringBuilder();
		buildStr.append(A);
		buildStr.append(B);
		return buildStr.toString();
	}	
	static public GameFinish verifyWord(PartyInfo PInfo, String szGivenWord, WordDecision DWord)
	{
		GameFinish gState = new GameFinish();

		if(checkWord(szGivenWord, DWord) == DECISIONS.WORD_CORRECT) 
			gState.hasWon = true;
		else 
			gState.hasWon = false;
	
		gState.szGivenWord = szGivenWord;
		gState.iLength = szGivenWord.length();

		return gState;
		
	}
	static public PointTable verifyLetter(PartyInfo PInfo, String szGivenWord, String szUsedLetters, LetterDecision gDec)
	{
		PointTable gNewStats = new PointTable();
		gNewStats.arrCorrect = new boolean[szGivenWord.length()];
		
		DECISIONS iRes = checkLetterCorrect(szGivenWord, gDec.Decision, szUsedLetters.toCharArray());
		if(iRes == DECISIONS.LETTER_CORRECT)
			{
				int[] Pos = retPosOfLetter(szGivenWord, gDec.Decision);
				for(int i = 0 ; i < Pos.length; i++)
					gNewStats.arrCorrect[Pos[i]] = true;
				
				gNewStats.isCorrect = true;
			} else if (iRes == DECISIONS.LETTER_USED)
			{
				gNewStats.isCorrect = false;
				// Nothin...
			} else { 
				PInfo.iFailures++;
				PInfo.iPoints -= 1;
				gNewStats.isCorrect = false;
			}
		
			gNewStats.iFailures = PInfo.iFailures;
			gNewStats.iPoints = PInfo.iPoints;
			
			return gNewStats;

	}
}
