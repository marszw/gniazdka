package tb.sockets.server;

import java.nio.ByteBuffer;
import java.util.Scanner;

import tb.sockets.utilities.GameFinish;
import tb.sockets.utilities.LetterDecision;
import tb.sockets.utilities.PartyInfo;
import tb.sockets.utilities.PointTable;
import tb.sockets.utilities.SocketManager;
import tb.sockets.utilities.WordDecision;

public class GameLogicServer {
	
	private String szWelcome = "Witaj w serwerze gry wisielca! \nPodaj na dobry pocz�tek wyraz, kt�ry klient ma odgadn��: "; 
	private String szTipInfo = "Ok, podaj jeszcze dla u�atwienia kr�tki opis tego wyrazu:";
	private String szWrongWord = "Zle podane s�owo! S�owo musi sie miescic od 1 do 32 znak�w!";
	private String szWrongTip = "Zle podany opis! Opis musi sie miescic od 1 do 64 znak�w!";
	private String szWaiting = "Czekamy na gracza...";
	private String szPort = "Podaj port serwera: ";
	private String szGivenWord;
	private String szUsedLetters = " ";
	
	private Server GameServer = null;
	
	private PartyInfo CurrentParty = null;
	
	private boolean hasStarted;
	
	private void ExamineGF(GameFinish gGF, WordDecision fDec)
	
	{
		println("#################################");
		println("Klient przeslal ostateczne s�owo!");
		println(fDec.szGivenWord);
		println("#################################");
		
		while(!GameServer.ClientWait());
		GameServer.send(SocketManager.GFToByte(gGF));
		
		if(gGF.hasWon)
			println("Klient wygra� rozgrywk�");
		else
			println("Klient przegra� rozgrywk�");	
	}
	private void ExaminePT(PointTable gPT)
	{
		while(!GameServer.ClientWait());
		
		if(gPT.iFailures > CurrentParty.maxFailures || CurrentParty.iPoints <= 0)
		{
			GameFinish sEnd = new GameFinish();
			sEnd.hasWon = false;
			sEnd.szGivenWord = szGivenWord;
			sEnd.iLength = szGivenWord.getBytes().length;
			
			GameServer.send(SocketManager.GFToByte(sEnd));

			hasStarted = false;	
			println("Klient przegra� rozgrywke.");		
		} else {
			println("###############################################");
			println(" ------- Aktualizacja statystyk klienta ------ ");
			println(Statistics.weldTwoStrings("Czy odgad� litere?: ", Boolean.toString(gPT.isCorrect)));
			println(Statistics.weldTwoStrings("Pora�ki: ", Integer.toString(gPT.iFailures)));
			println(Statistics.weldTwoStrings("Punkty: ", Integer.toString(gPT.iPoints)));
			println(Statistics.weldTwoStrings("Pozosta�e pr�by: ", Integer.toString(CurrentParty.maxFailures - gPT.iFailures)));
			println("###############################################");
			GameServer.send(SocketManager.PointTblToByt(gPT));
		}

	}
	private void exit()
	{
		GameServer.shutdown();
		GameServer = null;
	}
	private void gameLoop()
	{
		PointTable iRes;
		while(hasStarted) {
		while(!GameServer.ClientWait());
		
			ByteBuffer msg = GameServer.recv(64);
			
			switch(msg.get(0))
			{
			case 0x01: // PointTable
				break;
				
			case 0x02: //PartyInfo
				break;
			
			case 0x03: // Letter Decision
				LetterDecision gDec = SocketManager.BytToLDec(msg);
				iRes = Statistics.verifyLetter(CurrentParty, szGivenWord, szUsedLetters, gDec);
				println("Otrzymana litera: ");
				println(String.valueOf(gDec.Decision));
				ExaminePT(iRes);
				break;
				
			case 0x04: // Word Decision
			{
				WordDecision fWord = SocketManager.BytToDWord(msg);
				GameFinish gState = Statistics.verifyWord(CurrentParty, szGivenWord, fWord);
				ExamineGF(gState, fWord);
				hasStarted = false; //End...
			}
				break;
			case (byte) 0xAC:
				println("####################");
				println("Klient sie rozlaczyl");
				println("####################");
				hasStarted = false; //
				break;
			case (byte) 0xFF: //Heartbeat

				break;
				
			default:
				println("Received unknown message.");
				println(msg.array().toString());
				break;
			}
		
		}
		
		exit();
	}
	private void println(String txt)
	{
		System.out.println(txt);
		return;
	}
	
	public void start() { 
		Scanner wordScanner = new Scanner(System.in);
		
		if(GameServer == null)
			hasStarted = true;
		
		CurrentParty = new PartyInfo();
		
		println(szWelcome);
		szGivenWord = wordScanner.nextLine();
		
		while(szGivenWord.length() <= 0 || szGivenWord.length() > 128)
		{
			println(szWrongWord);
			szGivenWord = wordScanner.nextLine();
		}
		
		println(szTipInfo);
		CurrentParty.szTip = wordScanner.nextLine();
		while(CurrentParty.szTip.length() <= 0 || CurrentParty.szTip.length() > 256)
		{
			println(szWrongTip);
			CurrentParty.szTip = wordScanner.nextLine();
		}
		
		
		int Port = -1;
		println(szPort);
		while(Port < 0 || Port > 65535)
			Port = wordScanner.nextInt();
		
		GameServer = new Server(Port);
		wordScanner.close();
		wordScanner = null;
		println(szWaiting);
		
		Statistics.computePoints(CurrentParty, szGivenWord);
		System.out.println(SocketManager.returnSize(CurrentParty));
		
		while(!GameServer.ClientWait());
		GameServer.send(SocketManager.PartyToBytes(CurrentParty));
		println("Klient podlaczony! Wysylanie pierwszego pakietu!!");
		gameLoop();
	}

}
