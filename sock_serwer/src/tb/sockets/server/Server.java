package tb.sockets.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;

public class Server {
	
	//####################
	private int Port = 0;
	
	private ServerSocket Socket = null;
	private Socket ClientSocket = null;
	
	private boolean isInit = false;
	//##################

	public void shutdown()
	{
		try {
			ClientSocket.close();
			Socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		isInit = false;
	}
	Server(int port)
	{
		this.Port = port;
		try {
			Socket = new ServerSocket(port);
			isInit = true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			isInit = false;
		}
		
		if(!isInit)
		{
			System.err.println("Failed Server to Initialize!");
			printOut();
			return;
		}
	}

	private void printOut()
	{
		System.out.println("Port: " + Port );
		System.out.println("Socket: " + Socket.toString() );
		System.out.println("Init?: " + isInit );
		return;
	}
	
	public boolean valid()
	{
		return isInit;
	}
	
	public boolean send(ByteBuffer bytes)
	{
		boolean isSended = true;
		DataOutputStream Output = null;
		try {
			//
			Output = new DataOutputStream(ClientSocket.getOutputStream());
			Output.write(bytes.array());
			Output.flush();
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			isSended = false;
		} finally { 
			try {
				Output.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return isSended;
	}
	
	public ByteBuffer recv(int length)
	{

		DataInputStream sIn = null;
		ByteBuffer ToRet = ByteBuffer.allocate(length);
			
		try {
			sIn = new DataInputStream(ClientSocket.getInputStream());
			sIn.readFully(ToRet.array(), 0, ToRet.capacity());
		}catch (EOFException e)
		{}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally { 
			try {
				sIn.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		ToRet.limit(length);
		return ToRet;
		
	}
	public boolean ClientWait()
	{
		if(!isInit)
		{
			System.err.println("Server not properly initialized!");
			return false;
		}
		
		try {
			ClientSocket = Socket.accept();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("Failed to accept the client socket!");
			printOut();
			return false;
		}
		
		return true;
	}

}
