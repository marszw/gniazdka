package tb.sockets.utilities;


public class PartyInfo { 
	public final byte HEADER = 0x02; 
	public String szTip;
	public final byte StrSep1 = (byte)0xFF;
	public int iNmbLetters;
	public int iPoints;
	public int iFailures;
	public int maxFailures;
}