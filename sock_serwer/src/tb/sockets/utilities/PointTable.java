package tb.sockets.utilities;

public class PointTable
{
	public final byte HEADER = 0x01;
	public int iPoints;
	public int iFailures;
	public boolean isCorrect;
	public boolean[] arrCorrect;
}
