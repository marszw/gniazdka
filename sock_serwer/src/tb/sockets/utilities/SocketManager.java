package tb.sockets.utilities;

import java.nio.ByteBuffer;

public class SocketManager {
	static public int returnSize(Object data)
	{
		if(data instanceof GameFinish)
		{
			int Size = 1 + 1 + ((GameFinish) data).szGivenWord.length() + 4;
			return Size;
		}
		else if(data instanceof WordDecision)
		{
			int Size = ((WordDecision) data).szGivenWord.length() + 4 + 1;
			return Size;
		}
		else if(data instanceof LetterDecision)
		{
			int Size = 1 + 2;
			return Size;
		}
		else if(data instanceof PartyInfo) {
			PartyInfo tempData = (PartyInfo) data;
			int Size = 4*4 + 1 + 1 + 1 + tempData.szTip.length();
			return Size;
		} 
		else if (data instanceof PointTable)
		{
		
			int Size = 2 + 2*4 + ((PointTable) data).arrCorrect.length ;
			return Size;
		}
		
		return -1;
	}
	static public PartyInfo ByteToParty(ByteBuffer Buff)
	{
		if(Buff.get(0) != 0x02)
			return null;
		
		PartyInfo ToRet = new PartyInfo();
		
		int iSize, iPos;
		for(iPos = 1; iPos < Buff.capacity(); iPos++)
			if(Buff.get(iPos) == (byte) 0xFF)
				break;
		
		iSize = iPos;
		//iSize na pozycji koncowej pierwszego stringa
		byte[] szTemp = new byte[iSize - 1];
		
		for(iPos = 1; iPos < iSize; iPos++)
			szTemp[iPos - 1] = Buff.get(iPos); 
		
		ToRet.szTip = new String(szTemp);
		
		ToRet.iFailures = Buff.getInt(iPos + 1);
		ToRet.iPoints = Buff.getInt(iPos + 5);
		ToRet.maxFailures = Buff.getInt(iPos + 9);
		ToRet.iNmbLetters = Buff.getInt(iPos + 13);
		return ToRet;
	}
	static public ByteBuffer GFToByte(GameFinish gDec)
	{
		ByteBuffer ToRet = ByteBuffer.allocate(returnSize(gDec));
		ToRet.put(gDec.HEADER);
		
		if(gDec.hasWon)
			ToRet.put((byte) 01);
		else 
			ToRet.put((byte) 00);	
		
		ToRet.putInt(gDec.iLength);
		ToRet.put(gDec.szGivenWord.getBytes());
		
		return ToRet;
	}
	static public GameFinish ByteToGF(ByteBuffer Buff)
	{
		GameFinish ToRet = new GameFinish();
		
		if(Buff.get(1) == 01)
			ToRet.hasWon = true;
		else 
			ToRet.hasWon = false;	
		
		ToRet.iLength = Buff.getInt(2);
		
		byte szTemp[] = new byte[ToRet.iLength];
		
		for(int i = 0 ; i < szTemp.length ; i++)
			szTemp[i] = Buff.get(i + 6);
		
		ToRet.szGivenWord = new String(szTemp);
		return ToRet;
	}
	static public ByteBuffer PartyToBytes(PartyInfo gParty)
	{
		ByteBuffer ToRet = ByteBuffer.allocate(returnSize(gParty)); // 0A 0B FF A1 A3....
		ToRet.put(gParty.HEADER);
		ToRet.put(gParty.szTip.getBytes());
		ToRet.put(gParty.StrSep1);
		ToRet.putInt(gParty.iFailures);
		ToRet.putInt(gParty.iPoints);
		ToRet.putInt(gParty.maxFailures);
		ToRet.putInt(gParty.iNmbLetters);
		
		ToRet.limit(returnSize(gParty));
		return ToRet;
	}
	static public ByteBuffer PointTblToByt(PointTable gTable)
	{
		ByteBuffer ToRet = ByteBuffer.allocate(returnSize(gTable));
		ToRet.put(gTable.HEADER);
		ToRet.putInt(gTable.iFailures);
		ToRet.putInt(gTable.iPoints);
		
		if(gTable.isCorrect)
			ToRet.put((byte) 1);
		else
			ToRet.put((byte) 0);
		byte[] bytes = new byte[gTable.arrCorrect.length];
		
		for(int i = 0 ; i <gTable.arrCorrect.length;i++ )
		{
			if(gTable.arrCorrect[i])
				bytes[i] = 1;
			else
				bytes[i] = 0;
		}
		
		ToRet.put(bytes);
		
		return ToRet;
	}
	static public ByteBuffer LDecToByt(LetterDecision gDec)
	{
		ByteBuffer ToRet = ByteBuffer.allocate(returnSize(gDec));
		ToRet.put(gDec.HEADER);
		ToRet.putChar(gDec.Decision);
		return ToRet;
	}
	static public LetterDecision BytToLDec(ByteBuffer Buff)
	{
		LetterDecision ToRet = new LetterDecision();
		ToRet.Decision = Buff.getChar(1);
		return ToRet;
	}
	static public ByteBuffer DWordToByt(WordDecision gDec)
	{
		ByteBuffer ToRet = ByteBuffer.allocate(returnSize(gDec));
		byte Stringbyte[] = gDec.szGivenWord.getBytes();
		ToRet.put(gDec.HEADER);
		ToRet.putInt(Stringbyte.length);
		ToRet.put(Stringbyte);
		
		return ToRet;
	}
	static public WordDecision BytToDWord(ByteBuffer buff)
	{
		if(buff.get(0) != 0x04)
			return null;
		
		WordDecision ToRet = new WordDecision();
		
		ToRet.iLength = buff.getInt(1);
		byte szTemp[] = new byte[ToRet.iLength];
		
		for(int i = 0 ; i < szTemp.length ; i++)
			szTemp[i] = buff.get(i + 5);
		
		ToRet.szGivenWord = new String(szTemp);
		return ToRet;
	}
	static public PointTable BytToPointTbl(ByteBuffer Buff, int lengthofWord)
	{
		if(Buff.get(0) != 0x01)
			return null;
		
		PointTable ToRet = new PointTable();
		ToRet.iFailures = Buff.getInt(1);
		ToRet.iPoints = Buff.getInt(5);
		if(Buff.get(9) == 1)
			ToRet.isCorrect = true;
		else 
			ToRet.isCorrect = false;
		ToRet.arrCorrect = new boolean[lengthofWord];
		for(int i = 1 ; i <= lengthofWord; i++)
		{
			if(Buff.get(9 + i ) == 1)
				ToRet.arrCorrect[i - 1] = true;
			else
				ToRet.arrCorrect[i - 1] = false;
		}
		return ToRet;
	}

}
