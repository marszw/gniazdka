package tb.sockets.client.graph;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class HangMan extends JPanel {

	/**
	 * 
	 */
	
	private int iStage;
	private static final long serialVersionUID = 1L;

	//       ILOSC POPELNIONYCH BLEDOW / ILOSC MAX. DOP. BLEDOW
	public void updateStage(int iPiece, int iTotal)
	{
		double dPercentage = (iPiece - 1) * 100 / iTotal ;
		
		for(int i = 1 ; i < 12 ; i++)
		{
			if(i > 11)
			{
				iStage = 11;
				return;
			}
			if(dPercentage < (double)(100/11)*i) {
				iStage = i;
				return;
			}
		}
			
		iStage = -1;
	}
	
	@Override
	public void paintComponent(Graphics g) //DO RYSOWANIA
	{
		 int currWidth = getWidth();
		 int currHeight = getHeight();
		
		g.clearRect(0, 0, currWidth, currHeight);
		g.setColor(Color.RED);
		

		switch(iStage)
		{
		case 1:
			g.drawLine(currWidth/4, currHeight, currWidth/4, currHeight * 3/4);
			g.drawLine(currWidth * 3/4, currHeight, currWidth * 3/4, currHeight * 3/4);
			break;
		case 2:
			g.drawLine(currWidth/4, currHeight, currWidth/4, currHeight * 3/4);
			g.drawLine(currWidth * 3/4, currHeight, currWidth * 3/4, currHeight * 3/4);
			g.drawLine(currWidth/4, currHeight * 3/4, currWidth * 3/4, currHeight * 3/4);
			break;
		case 3:
			g.drawLine(currWidth/4, currHeight, currWidth/4, currHeight * 3/4);
			g.drawLine(currWidth * 3/4, currHeight, currWidth * 3/4, currHeight * 3/4);
			g.drawLine(currWidth/4, currHeight * 3/4, currWidth * 3/4, currHeight * 3/4);
			g.drawLine(currWidth*1/2, currHeight * 3/4, currWidth * 1/2, currHeight * 1/4);
			break;
		case 4:
			g.drawLine(currWidth/4, currHeight, currWidth/4, currHeight * 3/4);
			g.drawLine(currWidth * 3/4, currHeight, currWidth * 3/4, currHeight * 3/4);
			g.drawLine(currWidth/4, currHeight * 3/4, currWidth * 3/4, currHeight * 3/4);
			g.drawLine(currWidth*1/2, currHeight * 3/4, currWidth * 1/2, currHeight * 1/4);
			g.drawLine(currWidth * 1/2, currHeight * 1/4, currWidth * 1/2 * 1/2, currHeight * 1/4);
			break;
		case 5:
			g.drawLine(currWidth/4, currHeight, currWidth/4, currHeight * 3/4);
			g.drawLine(currWidth * 3/4, currHeight, currWidth * 3/4, currHeight * 3/4);
			g.drawLine(currWidth/4, currHeight * 3/4, currWidth * 3/4, currHeight * 3/4);
			g.drawLine(currWidth*1/2, currHeight * 3/4, currWidth * 1/2, currHeight * 1/4);
			g.drawLine(currWidth * 1/2, currHeight * 1/4, currWidth * 1/4, currHeight * 1/4);
			g.drawLine(currWidth * 1/4, currHeight * 1/4, currWidth * 1/4, currHeight * 1/4 * 4/3);
			break;
		case 6:
			g.drawLine(currWidth/4, currHeight, currWidth/4, currHeight * 3/4);
			g.drawLine(currWidth * 3/4, currHeight, currWidth * 3/4, currHeight * 3/4);
			g.drawLine(currWidth/4, currHeight * 3/4, currWidth * 3/4, currHeight * 3/4);
			g.drawLine(currWidth*1/2, currHeight * 3/4, currWidth * 1/2, currHeight * 1/4);
			g.drawLine(currWidth * 1/2, currHeight * 1/4, currWidth * 1/4, currHeight * 1/4);
			g.drawLine(currWidth * 1/4, currHeight * 1/4, currWidth * 1/4, currHeight * 1/4 * 4/3);
			g.drawOval(currWidth * 1/6, currHeight * 1/4 * 4/3, currWidth * 1/6, currHeight * 1/4 * 4/6);
			break;
		case 7:
			g.drawLine(currWidth/4, currHeight, currWidth/4, currHeight * 3/4);
			g.drawLine(currWidth * 3/4, currHeight, currWidth * 3/4, currHeight * 3/4);
			g.drawLine(currWidth/4, currHeight * 3/4, currWidth * 3/4, currHeight * 3/4);
			g.drawLine(currWidth*1/2, currHeight * 3/4, currWidth * 1/2, currHeight * 1/4);
			g.drawLine(currWidth * 1/2, currHeight * 1/4, currWidth * 1/4, currHeight * 1/4);
			g.drawLine(currWidth * 1/4, currHeight * 1/4, currWidth * 1/4, currHeight * 1/4 * 4/3);
			g.drawOval(currWidth * 1/6, currHeight * 1/4 * 4/3, currWidth * 1/6, currHeight * 1/4 * 4/6);
			g.drawLine(currWidth * 1/4, currHeight * 2/4 , currWidth * 1/4, currHeight * 2/4 * 4/3);
			break;
		case 8: 
			g.drawLine(currWidth/4, currHeight, currWidth/4, currHeight * 3/4);
			g.drawLine(currWidth * 3/4, currHeight, currWidth * 3/4, currHeight * 3/4);
			g.drawLine(currWidth/4, currHeight * 3/4, currWidth * 3/4, currHeight * 3/4);
			g.drawLine(currWidth*1/2, currHeight * 3/4, currWidth * 1/2, currHeight * 1/4);
			g.drawLine(currWidth * 1/2, currHeight * 1/4, currWidth * 1/4, currHeight * 1/4);
			g.drawLine(currWidth * 1/4, currHeight * 1/4, currWidth * 1/4, currHeight * 1/4 * 4/3);
			g.drawOval(currWidth * 1/6, currHeight * 1/4 * 4/3, currWidth * 1/6, currHeight * 1/4 * 4/6);
			g.drawLine(currWidth * 1/4, currHeight * 2/4 , currWidth * 1/4, currHeight * 2/4 * 4/3);
			g.drawLine(currWidth * 1/8, currHeight * 2/4 * 5/3 , currWidth * 2/8, currHeight * 2/4 * 4/3);
			break;
		case 9:
			g.drawLine(currWidth/4, currHeight, currWidth/4, currHeight * 3/4);
			g.drawLine(currWidth * 3/4, currHeight, currWidth * 3/4, currHeight * 3/4);
			g.drawLine(currWidth/4, currHeight * 3/4, currWidth * 3/4, currHeight * 3/4);
			g.drawLine(currWidth*1/2, currHeight * 3/4, currWidth * 1/2, currHeight * 1/4);
			g.drawLine(currWidth * 1/2, currHeight * 1/4, currWidth * 1/4, currHeight * 1/4);
			g.drawLine(currWidth * 1/4, currHeight * 1/4, currWidth * 1/4, currHeight * 1/4 * 4/3);
			g.drawOval(currWidth * 1/6, currHeight * 1/4 * 4/3, currWidth * 1/6, currHeight * 1/4 * 4/6);
			g.drawLine(currWidth * 1/4, currHeight * 2/4 , currWidth * 1/4, currHeight * 2/4 * 4/3);
			g.drawLine(currWidth * 1/8, currHeight * 2/4 * 5/3 , currWidth * 2/8, currHeight * 2/4 * 4/3);
			g.drawLine(currWidth * 1/4, currHeight * 2/4 * 4/3, currWidth * 3/8, currHeight * 2/4 * 4/3 * 6/5 );
			break;
		case 10:
			g.drawLine(currWidth/4, currHeight, currWidth/4, currHeight * 3/4);
			g.drawLine(currWidth * 3/4, currHeight, currWidth * 3/4, currHeight * 3/4);
			g.drawLine(currWidth/4, currHeight * 3/4, currWidth * 3/4, currHeight * 3/4);
			g.drawLine(currWidth*1/2, currHeight * 3/4, currWidth * 1/2, currHeight * 1/4);
			g.drawLine(currWidth * 1/2, currHeight * 1/4, currWidth * 1/4, currHeight * 1/4);
			g.drawLine(currWidth * 1/4, currHeight * 1/4, currWidth * 1/4, currHeight * 1/4 * 4/3);
			g.drawOval(currWidth * 1/6, currHeight * 1/4 * 4/3, currWidth * 1/6, currHeight * 1/4 * 4/6);
			g.drawLine(currWidth * 1/4, currHeight * 2/4 , currWidth * 1/4, currHeight * 2/4 * 4/3);
			g.drawLine(currWidth * 1/8, currHeight * 2/4 * 5/3 , currWidth * 2/8, currHeight * 2/4 * 4/3);
			g.drawLine(currWidth * 1/4, currHeight * 2/4 * 4/3, currWidth * 3/8, currHeight * 2/4 * 4/3 * 6/5 );
			g.drawLine(currWidth * 1/4, currHeight * 2/4 * 7/6, currWidth * 3/8, currHeight * 2/4 * 7/6 * 6/5 );
			break;
		
		case 11:
			g.drawLine(currWidth/4, currHeight, currWidth/4, currHeight * 3/4);
			g.drawLine(currWidth * 3/4, currHeight, currWidth * 3/4, currHeight * 3/4);
			g.drawLine(currWidth/4, currHeight * 3/4, currWidth * 3/4, currHeight * 3/4);
			g.drawLine(currWidth*1/2, currHeight * 3/4, currWidth * 1/2, currHeight * 1/4);
			g.drawLine(currWidth * 1/2, currHeight * 1/4, currWidth * 1/4, currHeight * 1/4);
			g.drawLine(currWidth * 1/4, currHeight * 1/4, currWidth * 1/4, currHeight * 1/4 * 4/3);
			g.drawOval(currWidth * 1/6, currHeight * 1/4 * 4/3, currWidth * 1/6, currHeight * 1/4 * 4/6);
			g.drawLine(currWidth * 1/4, currHeight * 2/4 , currWidth * 1/4, currHeight * 2/4 * 4/3);
			g.drawLine(currWidth * 1/8, currHeight * 2/4 * 5/3 , currWidth * 2/8, currHeight * 2/4 * 4/3);
			g.drawLine(currWidth * 1/4, currHeight * 2/4 * 4/3, currWidth * 3/8, currHeight * 2/4 * 4/3 * 6/5 );
			g.drawLine(currWidth * 1/4, currHeight * 2/4 * 7/6, currWidth * 3/8, currHeight * 2/4 * 7/6 * 6/5 );
			g.drawLine(currWidth * 1/8, currHeight * 2/4 * 7/6 * 7/6, currWidth * 2/8, currHeight * 2/4 * 7/6);
			break;
			
		default:
			g.fillRect(0, 0, getWidth(), getHeight());
			
		}
		
		
	}
}
