package tb.sockets.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.nio.ByteBuffer;

public class Client {
	
	private String IP;
	private int Port;
	private Socket Sock;
	
	Client(String IP, int Port)
	{
		this.IP = IP;
		this.Port = Port;
	}
	Client(int Port)
	{
		this.Port = Port;
		this.IP = "127.0.0.1";
	}
	
	public boolean Connect()
	{
		
		try {
			Sock = new Socket(IP, Port);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public boolean shutdown()
	{
		try {
			Sock.close();
			Sock = null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	public boolean send(ByteBuffer buff)
	{
		boolean isSended = true;
		DataOutputStream sOut = null ;
		try {
			sOut = new DataOutputStream(Sock.getOutputStream());
			sOut.write(buff.array());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			isSended = false;
		} finally {
			try {
				sOut.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return isSended;
	}
	
	
	public ByteBuffer recv(int length) {
		DataInputStream sIn = null;
		ByteBuffer ToRet = ByteBuffer.allocate(length);
		
		try {		
			sIn = new DataInputStream(Sock.getInputStream());
			sIn.readFully(ToRet.array(), 0, ToRet.capacity());
		}catch(EOFException e)
		{}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			//
			try {
				sIn.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		ToRet.flip();
		ToRet.limit(length);
		return ToRet;
	}
}
