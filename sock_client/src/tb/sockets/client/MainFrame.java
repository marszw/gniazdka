package tb.sockets.client;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.ParseException;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;

import tb.sockets.client.graph.HangMan;

public class MainFrame extends JFrame{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	private void showConnectError()
	{
		JOptionPane.showMessageDialog(this, "Nie uda�o si� po��czy� z serwerem.");
	}
	
	/**
	 * 
	 * Create the frame.
	 */
	public MainFrame() {
		setBounds(100, 100, 650, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblHost = new JLabel("Host:");
		lblHost.setBounds(10, 14, 26, 14);
		contentPane.add(lblHost);
		
		MaskFormatter IPFormatter = null;
		try {
			IPFormatter = new MaskFormatter("###.###.###.###");
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
		
		JFormattedTextField frmtdtxtfldIp = new JFormattedTextField(IPFormatter);


		frmtdtxtfldIp.setBounds(43, 11, 90, 20);
		frmtdtxtfldIp.setText("xxx.xxx.xxx.xxx");
		contentPane.add(frmtdtxtfldIp);
		
		JButton btnConnect = new JButton("Connect");
		btnConnect.setBounds(10, 70, 75, 23);
		contentPane.add(btnConnect);
		
		JFormattedTextField frmtdtxtfldXxxx = new JFormattedTextField();
		frmtdtxtfldXxxx.setText("xxxx"); // Port?
		frmtdtxtfldXxxx.setBounds(43, 39, 90, 20);
		contentPane.add(frmtdtxtfldXxxx);
		
		JLabel lblPort = new JLabel("Port:");
		lblPort.setBounds(10, 42, 26, 14);
		contentPane.add(lblPort);
		
		OrderPane panel = new OrderPane();
		panel.setBounds(145, 14, 487, 448);
		contentPane.add(panel);
		
		JLabel lblNotConnected = new JLabel("Not Connected");
		lblNotConnected.setForeground(new Color(255, 255, 255));
		lblNotConnected.setBackground(new Color(128, 128, 128));
		lblNotConnected.setOpaque(true);
		lblNotConnected.setBounds(10, 104, 123, 23);
		contentPane.add(lblNotConnected);
		
		HangMan CanvaHM = new HangMan();
		CanvaHM.setBounds(10, 150, 125, 125);
		contentPane.add(CanvaHM);
		setupListener(panel);
		btnConnect.addActionListener(new ActionListener() {
			
					@Override
			public void actionPerformed(ActionEvent e) {
				
				Client myClient = new Client(frmtdtxtfldIp.getText(), Integer.parseInt(frmtdtxtfldXxxx.getText()));
				
				if(myClient.Connect()) {
					lblNotConnected.setBackground(new Color(0, 255, 0));
					btnConnect.setEnabled(false);
					lblNotConnected.setText("Connected");
					panel.switchAll(true);
					panel.wakeUp(myClient, btnConnect, CanvaHM, lblNotConnected);
				} else { 
					showConnectError();
				};
				
			} 
			
		});
		
	}
private void disposeMe()
{
	this.dispose();
}
private void setupListener(OrderPane HPane) { 
		
		this.addWindowListener(new WindowListener() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				// TODO Auto-generated method stub
				HPane.terminateConnection();
				disposeMe();
			}

			@Override
			public void windowDeactivated(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowDeiconified(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowIconified(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowOpened(WindowEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub
				//
			}

			@Override
			public void windowClosed(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
		return;
	}
	

	

}
