package tb.sockets.client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.ByteBuffer;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import tb.sockets.client.graph.HangMan;
import tb.sockets.server.Statistics;
import tb.sockets.utilities.GameFinish;
import tb.sockets.utilities.LetterDecision;
import tb.sockets.utilities.PartyInfo;
import tb.sockets.utilities.PointTable;
import tb.sockets.utilities.SocketManager;
import tb.sockets.utilities.WordDecision;

public class OrderPane extends JPanel implements ActionListener {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;
	/**
	* Create the panel.
	*/
	private String szDescription = "Witaj w grze Wisielca. Po pod��czeniu si� do serwera, uzyskasz informacje ile liter zawiera "
	+ "dane s�owo zapisane tylko po stronie serwera. Program nie otrzymuje danego s�owa, tylko ilo�� liter. "
	+ "Podaj�c litere w polu nad przyciskiem \"Sprawdz litere\" wysy�ane jest zapytanie czy dana litera znajduj� si� w danym s�owie."
	+ " Je�li tak, zostanie ona odkryta. Gdy ju� odkryjesz, badz domy�lisz si� co to jest za s�owo, wpisz je w polu nad przyciskiem \"Sprawdz S�owo!\". "
	+ "Lecz uwa�aj, gdy� to jest jednostrza�owa czynno��. Wielko�� liter ma znaczenie, _ czasem mo�e oznacza� r�wnie� spacje. Przes�anie b��dnej litery"
	+ "skutkuje odj�ciem 1 punkta, przes�anie u�ytej ju� litery nie zmienia niczego. ";
	private String szRecvPoints= "Punkty dotychczas: "; 
	private String szFailPoints = "Nieudane pr�by: ";
	private String szTip = "Otrzymana podpowiedz: ";
	private String szCurrWord = "";
	private String szMaxFailures = "Max. pr�by: ";
	private JButton BCheckWord;
	private JButton BCheckLetter;
	private PartyInfo StartInfo;
	private int iNmbLetter;
	private HangMan ourHangMan;
	private JTextField TXTWholeWord;
	private JTextField TXTLetter;
	
	private LetterDecision LDec;
	private JLabel CntStat;
	private JButton BHelp;
	private JLabel LDescription;
	private JLabel LReceviedTip;
	private JLabel LReceivedWord;
	private JLabel LReceivedPoints;
	private JLabel LReceivedFailures;
	private JLabel LMaxFailures;
	private JButton BReturn;
	private Client ConnectedSocket;
	
	public void terminateConnection()
	{
		if(ConnectedSocket == null)
			return;
		
		ConnectedSocket.Connect();
		byte[] bytes = new byte[1];
		bytes[0] = (byte) 0xAC; // Abort Connection
		
		ConnectedSocket.send(ByteBuffer.wrap(bytes));
		
		GameFinish GFAborted = new GameFinish();
		GFAborted.szGivenWord = "PRZERWANO PO��CZENIE";
		GFAborted.iLength = GFAborted.szGivenWord.length();
		GFAborted.hasWon = false;
		shutdown(GFAborted);
		return;
	}
	private void shutdown(GameFinish state)
	{
		String szMsg;
		if(state.hasWon) {
			szMsg = "Wygra�e�!!!";
			LReceivedPoints.setBackground(Color.GREEN);
			LReceivedPoints.setText(szMsg);
		}
		else {
			szMsg = "Przegra�e�!!!";
			LReceivedPoints.setBackground(Color.RED);
			LReceivedPoints.setText(szMsg);
		}		
		
		JOptionPane.showMessageDialog(this, szMsg);
		LReceivedWord.setText(state.szGivenWord);
		ConnectedSocket.shutdown();
		CntStat.setText("Not Connected");
		CntStat.setBackground(new Color(128,128,128));
		ConnectedSocket = null;
		System.gc();
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				switchAll(false);
				BReturn.setEnabled(true);
			}
			
		});
		return;
	} //
	private void refreshStats(PointTable OutCome)
	{
		ourHangMan.updateStage(OutCome.iFailures, StartInfo.maxFailures);
		ourHangMan.repaint();
		char[] chCurrWord = szCurrWord.toCharArray();
		int iFactor = -1;
		LReceivedPoints.setText(Statistics.weldTwoStrings(szRecvPoints, Integer.toString(OutCome.iPoints)));
		LReceivedFailures.setText(Statistics.weldTwoStrings(szFailPoints, Integer.toString(OutCome.iFailures)));
		if(!OutCome.isCorrect) 
			return;
		
		for(int i = 1 ; i < chCurrWord.length; i++)
		{
			if(i % 3 == 1) 
			{
				iFactor++;
				if(OutCome.arrCorrect[iFactor])
				chCurrWord[i] = LDec.Decision;
			}
		}
		
		szCurrWord = new String(chCurrWord);
		LReceivedWord.setText(szCurrWord);
		
	}
	private void generateControls()
	{
		String test = "B R A K  S � O W A";
		setLayout(new BorderLayout());
		BCheckWord = new JButton("Sprawdz S�owo!");
		BCheckLetter = new JButton("Sprawdz litere!");
		BHelp = new JButton("Help");
		TXTWholeWord = new JTextField((int)(test.length()));
		TXTLetter = new JTextField(1);
		LDescription = new JLabel(szDescription);
		LReceviedTip = new JLabel(szTip);
		LReceivedWord = new JLabel(test);
		LReceivedWord.setFont(TXTWholeWord.getFont());
		LReceivedPoints = new JLabel(szRecvPoints);
		LReceivedFailures = new JLabel(szFailPoints);
		LMaxFailures = new JLabel(szMaxFailures);
		
		JPanel North = new JPanel();
		North.setBackground(new Color(255, 255, 240));
		North.setLayout(new GridLayout(4,1,5,5));
		North.add(LReceivedWord);
		North.add(BHelp);
		North.add(TXTLetter);
		North.add(BCheckLetter);
		add(North, BorderLayout.NORTH);
		

		JPanel South = new JPanel();
		South.setBackground(new Color(255, 255, 240));
		South.setLayout(new GridLayout(2,1,5,5));
		South.add(TXTWholeWord);
		TXTLetter.setBounds(new Rectangle(2,2));
		South.add(BCheckWord);
		
		add(South, BorderLayout.SOUTH);
		
		JPanel Center = new JPanel();
		Center.setBackground(new Color(255, 255, 240));
		Center.setLayout(new GridLayout(4,1,5,5));
		Center.add(LReceviedTip);
		Center.add(LReceivedPoints);
		Center.add(LReceivedFailures);
		Center.add(LMaxFailures);
		
		//Center.add(LDescription);
		LDescription.setPreferredSize(getMaximumSize());
		add(Center, BorderLayout.CENTER);
		
		BCheckWord.addActionListener(this);
		BCheckLetter.addActionListener(this);
		BHelp.addActionListener(this);
	}
	private void readMsg()
	{
		ConnectedSocket.Connect();
		ByteBuffer msg = ConnectedSocket.recv(64);
		
		switch(msg.get(0))
		{
		case 0x01:
			refreshStats(SocketManager.BytToPointTbl(msg, iNmbLetter));
			break;
			
		case 0x04:
			break;
			
		case (byte) 0xF1:
			GameFinish nState = SocketManager.ByteToGF(msg);
			shutdown(nState);
			break;
			
		default:
			System.out.println("Received smth letter dic");
			break;
			
			
		}
	}

	public void switchAll(boolean State)
	{
		TXTWholeWord.setEditable(State);
		TXTLetter.setEditable(State);
		
		BCheckWord.setEnabled(State);
		BCheckLetter.setEnabled(State);
	}	
	
	public void wakeUp(HangMan gHM)
	{
		StartInfo = new PartyInfo();
		
		StartInfo.maxFailures = 16;
		StartInfo.iFailures = 0;
		ourHangMan = gHM;
	}
	public void wakeUp(Client gSocket, JButton gReturn, HangMan gHM, JLabel ConnectionStatus)
	{
		CntStat = ConnectionStatus;
		ourHangMan = gHM;
		BReturn = gReturn;
		ConnectedSocket = gSocket;
		ByteBuffer bytesStart = ConnectedSocket.recv(64);
		
		StartInfo = SocketManager.ByteToParty(bytesStart);
		iNmbLetter = StartInfo.iNmbLetters;
		szCurrWord = Statistics.generateDashes(StartInfo.iNmbLetters);
		LReceivedWord.setText(szCurrWord);
		LReceivedPoints.setText(Statistics.weldTwoStrings(szRecvPoints, Integer.toString(StartInfo.iPoints)));
		LReceviedTip.setText(Statistics.weldTwoStrings(szTip, StartInfo.szTip));
		LMaxFailures.setText(Statistics.weldTwoStrings(szMaxFailures, Integer.toString(StartInfo.maxFailures)));
	}
	public OrderPane() {
		setBackground(new Color(255, 255, 240));
		generateControls();
		switchAll(false);
		
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
		if(arg0.getSource().equals(BHelp))
		{
			JOptionPane.showMessageDialog(this, szDescription);
		}
		else if(arg0.getSource().equals(BCheckLetter))
		{
			 LDec = new LetterDecision();
			
			try {
				LDec.Decision = TXTLetter.getText().charAt(0);
			} catch(IndexOutOfBoundsException e)
			{
				return;
			}
			
			ConnectedSocket.Connect();
			ConnectedSocket.send(SocketManager.LDecToByt(LDec));
			System.out.println("Sending letter...");
			readMsg(); 
			
			
		} else if (arg0.getSource().equals(BCheckWord)) {
			ConnectedSocket.Connect();
			WordDecision WDec = new WordDecision();
			WDec.szGivenWord = TXTWholeWord.getText();
			WDec.iLength = WDec.szGivenWord.length();
			
			System.out.println("Sending word...");
			ConnectedSocket.send(SocketManager.DWordToByt(WDec));
			readMsg();
		}
		
	}
}
